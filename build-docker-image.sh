#!/bin/sh

# SPDX-FileCopyrightText: Chris Pressey, the original author of this work, has dedicated it to the public domain.
# For more information, please refer to <https://unlicense.org/>
# SPDX-License-Identifier: Unlicense

# After building the image, you can run yastasoti from within it like:
#
# docker run --user $(id -u):$(id -g) -i -t -v "${PWD}:/mnt/host/" yastasoti \
#        yastasoti links.json --archive-to=out/
#

rm -rf script/*.pyc script/__pycache__
docker container prune
docker rmi catseye/yastasoti:0.4
docker rmi yastasoti
docker build -t yastasoti .
docker tag yastasoti catseye/yastasoti:0.4
